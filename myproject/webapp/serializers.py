from rest_framework import serializers
from . models import employees  , Role

class employeesSerializer(serializers.ModelSerializer):
	"""docstring for employeeSerializer"""
	class Meta:
		model = employees
		fields = ('User','First_Name')	


class RoleSerializer(serializers.ModelSerializer):
	"""docstring for employeeSerializer"""
	user=employeesSerializer()
	class Meta:
		
		model= Role
		fields = ('Role','user')
