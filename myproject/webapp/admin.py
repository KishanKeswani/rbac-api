from django.contrib import admin

# Register your models here.
from . models import employees , Role

admin.site.register(employees)
admin.site.register(Role)